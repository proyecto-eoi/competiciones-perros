package com.perroscompeticiones.proyecto.servicios;

import java.util.List;

import com.perroscompeticiones.proyecto.entidades.Modalidad;

public interface IModalidadService {
	public List<Modalidad> findAll();
}