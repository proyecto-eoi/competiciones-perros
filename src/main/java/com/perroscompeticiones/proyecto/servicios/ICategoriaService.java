package com.perroscompeticiones.proyecto.servicios;

import java.util.List;

import com.perroscompeticiones.proyecto.entidades.Categoria;

public interface ICategoriaService {

	public List<Categoria> findAll();
	public Categoria findById(Integer id);
}
