package com.perroscompeticiones.proyecto.servicios;

import java.util.List;

import com.perroscompeticiones.proyecto.entidades.Tiene;

public interface ITieneService {

	public List<Tiene> findAll();

	public Tiene findById(Integer id);
}
