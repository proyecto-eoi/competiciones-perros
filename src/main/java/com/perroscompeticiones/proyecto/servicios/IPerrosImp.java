package com.perroscompeticiones.proyecto.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perroscompeticiones.proyecto.entidades.Perros;
import com.perroscompeticiones.proyecto.repositorios.IPerros;

@Service
public class IPerrosImp implements IPerrosService {

	@Autowired
	private IPerros perrosDao;

	@Override
	public Perros save(Perros perro) {
		return perrosDao.save(perro);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Perros> findAll() {
		return perrosDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Perros findById(Integer id) {
		return perrosDao.findById(id).orElse(null);
	}

	@Override
	public void delete(Perros perro) {
		perrosDao.delete(perro);
	}
	
	@Override
	@Transactional
	public void deleteById(Integer id) {
		perrosDao.delete(findById(id));
	}
}
