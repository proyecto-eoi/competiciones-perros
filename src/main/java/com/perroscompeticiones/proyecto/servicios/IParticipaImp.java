package com.perroscompeticiones.proyecto.servicios;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perroscompeticiones.proyecto.entidades.Participa;
import com.perroscompeticiones.proyecto.repositorios.IParticipa;

@Service
public class IParticipaImp implements IParticipaService {

	@Autowired
	private IParticipa participaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Participa> findAll() {
		return participaDao.findAll();
	}

	@Override
	public Participa findById(Integer id) {
		return participaDao.findById(id).orElse(null);
	}

	@Override
	public Participa save(Participa participa) {
		return participaDao.save(participa);
	}

	@Override
	public void delete(Participa participa) {
		participaDao.delete(participa);
	}

	@Override
	public void deleteById(Integer id) {
		participaDao.delete(findById(id));
	}

	@Override
	public List<Participa> findByIdModalidad(Integer id) {
		List<Participa> lista = participaDao.findAll();
		return lista.stream().filter(f -> f.getModalidad().getId() == id).collect(Collectors.toList());
	}
}
