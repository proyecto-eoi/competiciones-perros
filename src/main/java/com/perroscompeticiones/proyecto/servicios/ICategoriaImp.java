package com.perroscompeticiones.proyecto.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perroscompeticiones.proyecto.entidades.Categoria;
import com.perroscompeticiones.proyecto.repositorios.ICategoria;

@Service
public class ICategoriaImp implements ICategoriaService {

	@Autowired
	private ICategoria categoriaDao;

	@Override
	@Transactional(readOnly = true)
	public Categoria findById(Integer id) {
		return categoriaDao.findById(id).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Categoria> findAll() {
		return categoriaDao.findAll();
	}
}
