package com.perroscompeticiones.proyecto.servicios;

import java.util.List;

import com.perroscompeticiones.proyecto.entidades.Perros;

public interface IPerrosService {
	public List<Perros> findAll();
	public Perros findById(Integer id);
	public Perros save(Perros perro);
	public void delete(Perros perro);
	public void deleteById(Integer id);
}