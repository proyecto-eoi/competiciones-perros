package com.perroscompeticiones.proyecto.servicios;

import java.util.List;
import com.perroscompeticiones.proyecto.entidades.Participa;

public interface IParticipaService {
	public List<Participa> findAll();
	public Participa findById(Integer id);
	public Participa save(Participa participa);
	public void delete(Participa participa);
	public void deleteById(Integer id);
	public List<Participa> findByIdModalidad(Integer id);
}