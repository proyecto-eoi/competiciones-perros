package com.perroscompeticiones.proyecto.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perroscompeticiones.proyecto.entidades.Modalidad;
import com.perroscompeticiones.proyecto.repositorios.IModalidad;

@Service
public class IModalidadImp implements IModalidadService {

	@Autowired
	private IModalidad modalidadDao;

	@Override
	@Transactional(readOnly = true)
	public List<Modalidad> findAll() {
		return modalidadDao.findAll();
	}
}