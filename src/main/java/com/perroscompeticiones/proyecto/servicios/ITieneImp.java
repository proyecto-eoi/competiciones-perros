package com.perroscompeticiones.proyecto.servicios;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.perroscompeticiones.proyecto.entidades.Tiene;
import com.perroscompeticiones.proyecto.repositorios.ITiene;

@Service
public class ITieneImp implements ITieneService{
	
	@Autowired
	private ITiene tieneDao;	
	
	@Override
	@Transactional(readOnly = true)
	public List<Tiene> findAll() {
		return tieneDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Tiene findById(Integer id) {
		return tieneDao.findById(id).orElse(null);
	}

}
