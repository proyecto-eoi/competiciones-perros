package com.perroscompeticiones.proyecto.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.perroscompeticiones.proyecto.entidades.Categoria;

@Repository
public interface ICategoria extends JpaRepository<Categoria,Integer>{

}
