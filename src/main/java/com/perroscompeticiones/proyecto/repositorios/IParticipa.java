package com.perroscompeticiones.proyecto.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.perroscompeticiones.proyecto.entidades.Participa;

@Repository
@RepositoryRestResource(path="participa", collectionResourceRel="participa")
public interface IParticipa extends JpaRepository<Participa, Integer>{

}
