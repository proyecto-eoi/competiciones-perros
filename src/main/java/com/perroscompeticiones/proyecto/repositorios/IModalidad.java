package com.perroscompeticiones.proyecto.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.perroscompeticiones.proyecto.entidades.Modalidad;

@Repository
@RepositoryRestResource(path="modalidad", collectionResourceRel="modalidad")
public interface IModalidad extends JpaRepository<Modalidad,Integer>{

}