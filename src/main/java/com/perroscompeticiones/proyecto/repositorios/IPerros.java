package com.perroscompeticiones.proyecto.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.perroscompeticiones.proyecto.entidades.Perros;

@Repository
@RepositoryRestResource(path="perros", collectionResourceRel="perros")
public interface IPerros extends JpaRepository<Perros,Integer>{

}
