package com.perroscompeticiones.proyecto.repositorios;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import com.perroscompeticiones.proyecto.entidades.Tiene;

@Repository
@RepositoryRestResource(path="tiene", collectionResourceRel="tiene")
public interface ITiene extends JpaRepository<Tiene,Integer>{

}
