package com.perroscompeticiones.proyecto.entidades;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "categoria")
public class Categoria implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String nombre;
	private Set<Perros> perros = new HashSet<Perros>(0);
	private Set<Tiene> tiene = new HashSet<Tiene>(0);

	public Categoria() {
	}

	public Categoria(int id) {
		this.id = id;
	}

	public Categoria(int id, String nombre, Set<Perros> perros, Set<Tiene> tiene) {
		this.id = id;
		this.nombre = nombre;
		this.perros = perros;
		this.tiene = tiene;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "categoria")
	public Set<Perros> getPerros() {
		return this.perros;
	}

	public void setPerros(Set<Perros> perros) {
		this.perros = perros;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "categoria")
	public Set<Tiene> getTiene() {
		return this.tiene;
	}

	public void setTiene(Set<Tiene> tiene) {
		this.tiene = tiene;
	}

	@Override
	public String toString() {
		return "id=" + id + ", nombre=" + nombre;
	}
}
