package com.perroscompeticiones.proyecto.entidades;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tiene")
public class Tiene implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private Categoria categoria;
	private Modalidad modalidad;

	public Tiene() {
	}

	public Tiene(int id, Categoria categoria, Modalidad modalidad) {
		this.id = id;
		this.categoria = categoria;
		this.modalidad = modalidad;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_categoria", nullable = false)
	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_modalidad", nullable = false)
	public Modalidad getModalidad() {
		return this.modalidad;
	}

	public void setModalidad(Modalidad modalidad) {
		this.modalidad = modalidad;
	}

	@Override
	public String toString() {
		return "id=" + id + ", categoria=" + categoria + ", modalidad=" + modalidad;
	}
}
