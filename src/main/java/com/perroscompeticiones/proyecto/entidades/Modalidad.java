package com.perroscompeticiones.proyecto.entidades;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "modalidad")
public class Modalidad implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private String nombre;
	private String url;
	private Set<Tiene> tiene = new HashSet<Tiene>(0);
	private Set<Participa> participa = new HashSet<Participa>(0);

	public Modalidad() {
	}

	public Modalidad(int id) {
		this.id = id;
	}

	public Modalidad(int id, String nombre, String url, Set<Tiene> tiene, Set<Participa> participa) {
		this.id = id;
		this.nombre = nombre;
		this.tiene = tiene;
		this.participa = participa;
		this.url = url;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "url")
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "modalidad")
	public Set<Tiene> getTiene() {
		return this.tiene;
	}

	public void setTiene(Set<Tiene> tiene) {
		this.tiene = tiene;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "modalidad")
	public Set<Participa> getParticipa() {
		return this.participa;
	}

	public void setParticipa(Set<Participa> participa) {
		this.participa = participa;
	}

	@Override
	public String toString() {
		return "id=" + id + ", nombre=" + nombre + ", url=" + url;
	}
}
