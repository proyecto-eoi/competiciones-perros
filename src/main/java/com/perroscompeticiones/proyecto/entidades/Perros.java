package com.perroscompeticiones.proyecto.entidades;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "perros")
public class Perros implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private Categoria categoria;
	private String nombre;
	private String raza;
	private String url;
	private Set<Participa> participa = new HashSet<Participa>(0);

	public Perros() {
	}

	public Perros(int id) {
		this.id = id;
	}

	public Perros(int id, Categoria categoria, String nombre, String raza, String url, Set<Participa> participa) {
		this.id = id;
		this.categoria = categoria;
		this.nombre = nombre;
		this.raza = raza;
		this.url = url;
		this.participa = participa;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_categoria")
	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "raza")
	public String getRaza() {
		return this.raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	@Column(name = "url")
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "perros")
	public Set<Participa> getParticipa() {
		return this.participa;
	}

	public void setParticipa(Set<Participa> participa) {
		this.participa = participa;
	}

	@Override
	public String toString() {
		return "id=" + id + ", categoria=" + categoria + ", nombre=" + nombre + ", raza=" + raza + ", url=" + url;
	}
}