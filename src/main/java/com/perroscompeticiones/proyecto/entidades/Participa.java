package com.perroscompeticiones.proyecto.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "participa")
public class Participa implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private Modalidad modalidad;
	private Perros perros;
	private Date fecha;

	public Participa() {
	}

	public Participa(int id, Modalidad modalidad, Perros perros) {
		this.id = id;
		this.modalidad = modalidad;
		this.perros = perros;
	}

	public Participa(int id, Modalidad modalidad, Perros perros, Date fecha) {
		this.id = id;
		this.modalidad = modalidad;
		this.perros = perros;
		this.fecha = fecha;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_modalidad", nullable = false)
	public Modalidad getModalidad() {
		return this.modalidad;
	}

	public void setModalidad(Modalidad modalidad) {
		this.modalidad = modalidad;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_perro", nullable = false)
	public Perros getPerros() {
		return this.perros;
	}

	public void setPerros(Perros perros) {
		this.perros = perros;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha", length = 13)
	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "id=" + id + ", modalidad=" + modalidad + ", perros=" + perros + ", fecha=" + fecha;
	}
}
