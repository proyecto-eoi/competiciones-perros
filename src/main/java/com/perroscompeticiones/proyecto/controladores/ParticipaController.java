package com.perroscompeticiones.proyecto.controladores;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.perroscompeticiones.proyecto.dto.ParticipaDto;
import com.perroscompeticiones.proyecto.entidades.Participa;
import com.perroscompeticiones.proyecto.servicios.IParticipaService;

@CrossOrigin(origins = { "*" })
@RestController
public class ParticipaController {

	@Autowired
	private IParticipaService participaService;

	/**
	 * Metodo get donde obtenemos todas las participaciones de la bd.
	 * 
	 * @return lista(Participa)
	 */
	@GetMapping("/participa")
	public List<Participa> index() {
		List<Participa> participaciones = participaService.findAll();
		return participaciones;
	}

	@GetMapping("/participa/filtered-by-modalidad/{id}")
	public ResponseEntity<?> modalidad(@PathVariable Integer id) {
		List<Participa> participaciones = participaService.findByIdModalidad(id);
		List<ParticipaDto> participaDtoList = new ArrayList<ParticipaDto>();
		Map<String, Object> respuesta = new HashMap<String, Object>();
		participaciones.forEach(
				item -> participaDtoList.add(new ParticipaDto(item.getId(), item.getPerros(), item.getFecha())));
		if (participaciones.isEmpty()) { // Hemos buscado un id que no existe
			respuesta.put("mensaje", "La modalidad buscada no existe");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		// Hemos buscado el ID y SI que existe
		return new ResponseEntity<List<ParticipaDto>>(participaDtoList, HttpStatus.OK);
	}

	@GetMapping("/participa/{id}")
	public ResponseEntity<?> mostrarParticipa(@PathVariable Integer id) {

		Participa participa = null;
		Map<String, Object> respuesta = new HashMap<String, Object>();

		try {
			participa = participaService.findById(id);
		} catch (Exception e) { // Base de datos inaccesible
			respuesta.put("mensaje", "Error al realizar la consulta sobre la base de datos");
			respuesta.put("error", e.getMessage().concat(" : ").concat(e.getStackTrace().toString()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (participa == null) { // Hemos buscado un id que no existe
			respuesta.put("mensaje", "La participacion buscada no existe");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		// Hemos buscado el ID y SI que existe
		return new ResponseEntity<Participa>(participa, HttpStatus.OK);
	}

	/**
	 * Metodo post donde insertamos una nueva participacion de un perro en una
	 * modalidad en la tabla participa. Si se introduce el id de modalidad o perro
	 * erroneo, salta la excepcion, en el otro caso, la peticion se completa.
	 * 
	 * @param participa(Participa), body de la peticion
	 * @return Respuesta del servidor
	 */
	@PostMapping("/participa")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@Valid @RequestBody Participa participa) {
		Map<String, Object> respuesta = new HashMap<String, Object>();
		try {
			participaService.save(participa);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al intentar insertar sobre la base de datos");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		respuesta.put("mensaje", "Nueva participacion creada correctamente");
		respuesta.put("participa", participa);

		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	/**
	 * Metodo delete donde borramos una participacion de un perro en una modalidad
	 * mediante el id de la tabla participa. Si se introduce un id erroneo, salta la
	 * excepcion, en el otro caso, la peticion se completa.
	 * 
	 * @param id de la participacion del perro a eliminar
	 * @return Respuesta del servidor
	 */
	@DeleteMapping("/participa/{id}")
	public ResponseEntity<?> borrar(@PathVariable Integer id) {
		Map<String, Object> respuesta = new HashMap<String, Object>();
		try {
			participaService.deleteById(id);
		} catch (Exception e) { // El id no está para borrar
			respuesta.put("mensaje", "Error al borrar la participacion, no existe");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		respuesta.put("mensaje", "La participacion con id " + id + " ha sido borrada con éxito");

		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}
}
