package com.perroscompeticiones.proyecto.controladores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.perroscompeticiones.proyecto.entidades.Tiene;
import com.perroscompeticiones.proyecto.servicios.ITieneService;

@CrossOrigin(origins = { "*" })
@RestController
public class TieneController {

	@Autowired
	private ITieneService tieneService;

	@GetMapping("/tiene")
	public List<Tiene> index() {
		return tieneService.findAll();
	}

	@GetMapping("/tiene/{id}")
	public ResponseEntity<?> mostrarCatMod(@PathVariable Integer id) {

		Tiene tiene = null;
		Map<String, Object> respuesta = new HashMap<String, Object>();

		try {
			tiene = tieneService.findById(id);
		} catch (Exception e) {
			respuesta.put("mensaje", "Error al realizar la consulta sobre la base de datos");
			respuesta.put("error", e.getMessage().concat(" : ").concat(e.getStackTrace().toString()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (tiene == null) {
			respuesta.put("mensaje", "La relacion no existe");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		return new ResponseEntity<Tiene>(tiene, HttpStatus.OK);
	}
}