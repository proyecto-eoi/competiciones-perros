package com.perroscompeticiones.proyecto.controladores;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.perroscompeticiones.proyecto.entidades.Modalidad;
import com.perroscompeticiones.proyecto.servicios.IModalidadService;

@CrossOrigin(origins= {"*"})
@RestController
public class ModalidadController {

	@Autowired
	private IModalidadService modalidadService;

	@GetMapping("/modalidad")
	public List<Modalidad> index() {
		return modalidadService.findAll();
	}
}