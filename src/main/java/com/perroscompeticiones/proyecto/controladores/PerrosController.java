package com.perroscompeticiones.proyecto.controladores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.perroscompeticiones.proyecto.entidades.Perros;
import com.perroscompeticiones.proyecto.servicios.IPerrosService;

@CrossOrigin(origins = { "*" })
@RestController
public class PerrosController {

	@Autowired
	private IPerrosService perrosService;

	/**
	 * Se devuelven todos los perros que hay en la BD.
	 * 
	 * @return lista(Perros) de todos los perros existentes
	 */
	@GetMapping("/perros")
	public List<Perros> index() {
		return perrosService.findAll();
	}

	/**
	 * Metodo donde insertamos un nuevo perro en la tabla perros. Si se introduce
	 * una categoria erronea, salta la excepcion, en el otro caso, la peticion se
	 * completa.
	 * 
	 * @param perro(Perros), body de la peticion
	 * @return Respuesta del servidor
	 */
	@PostMapping("/perros")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> create(@Valid @RequestBody Perros perro) {
		Map<String, Object> respuesta = new HashMap<String, Object>();

		try {
			perrosService.save(perro);
		} catch (DataAccessException e) {
			respuesta.put("mensaje", "Error al intentar insertar sobre la base de datos");
			respuesta.put("error", e.getMessage().concat(":").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		respuesta.put("mensaje", "Perro creado correctamente");
		respuesta.put("perro", perro);

		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.CREATED);
	}

	/**
	 * Metodo donde borramos un perro mediante su id en la tabla perros. Si se
	 * introduce un id erroneo, salta la excepcion, en el otro caso, la peticion se
	 * completa.
	 * 
	 * @param perro(Perros), id del perro a eliminar
	 * @return Respuesta del servidor
	 */
	@DeleteMapping("/perros/{id}")
	public ResponseEntity<?> borrar(@PathVariable Integer id) {
		Map<String, Object> respuesta = new HashMap<String, Object>();
		try {
			perrosService.deleteById(id);
		} catch (Exception e) { // El id no está para borrar
			respuesta.put("mensaje", "Error al borrar el perro, no existe");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			respuesta.put("mensaje", "El perro con id " + id + " ha sido borrado con éxito");
		}

		return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.OK);
	}
}