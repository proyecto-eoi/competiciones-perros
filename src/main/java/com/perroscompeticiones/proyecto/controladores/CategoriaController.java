package com.perroscompeticiones.proyecto.controladores;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.perroscompeticiones.proyecto.entidades.Categoria;
import com.perroscompeticiones.proyecto.servicios.ICategoriaService;

@CrossOrigin(origins = { "*" })
@RestController
public class CategoriaController {

	@Autowired
	private ICategoriaService categoriaService;

	@GetMapping("/categorias")
	public List<Categoria> index() {
		return categoriaService.findAll();
	}

	@GetMapping("/categorias/{id}")
	public ResponseEntity<?> mostrarCategoria(@PathVariable Integer id) {

		Categoria categoria = null;
		Map<String, Object> respuesta = new HashMap<String, Object>();

		try {
			categoria = categoriaService.findById(id);
		} catch (Exception e) { // Base de datos inaccesible
			respuesta.put("mensaje", "Error al realizar la consulta sobre la base de datos");
			respuesta.put("error", e.getMessage().concat(" : ").concat(e.getStackTrace().toString()));
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (categoria == null) { // Hemos buscado un id que no existe
			respuesta.put("mensaje", "La categoria buscada no existe");
			return new ResponseEntity<Map<String, Object>>(respuesta, HttpStatus.NOT_FOUND);
		}

		// Hemos buscado el ID y SI que existe
		return new ResponseEntity<Categoria>(categoria, HttpStatus.OK);
	}
}