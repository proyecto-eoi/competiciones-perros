package com.perroscompeticiones.proyecto.dto;

import java.io.Serializable;
import java.util.Date;

import com.perroscompeticiones.proyecto.entidades.Categoria;
import com.perroscompeticiones.proyecto.entidades.Perros;

public class ParticipaDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private int id;
	private Perros perros;
	private Categoria categoria;
	private Date fecha;

	public ParticipaDto() {

	}

	public ParticipaDto(int id, Perros perros, Date fecha) {
		this.id = id;
		this.perros = perros;
		this.categoria = this.perros.getCategoria();
		this.fecha = fecha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Perros getPerros() {
		return perros;
	}

	public void setPerros(Perros perros) {
		this.perros = perros;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "id:" + id + ", perros:" + perros + ", categoria:" + categoria + ",fecha:" + fecha;
	}

}
